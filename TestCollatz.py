"""
Unit Tests
"""

#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    """
    Define the test cases
    """
    # ----
    # read
    # ----

    def test_read(self):
        """
        Rest test
        """
        s_num = "1 10\n"
        i_num, j_num = collatz_read(s_num)
        self.assertEqual(i_num, 1)
        self.assertEqual(j_num, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        """
        Test 1
        """
        v_num = collatz_eval(1, 10)
        self.assertEqual(v_num, 20)

    def test_eval_2(self):
        """
        Test 2
        """
        v_num = collatz_eval(100, 200)
        self.assertEqual(v_num, 125)

    def test_eval_3(self):
        """
        Test 3
        """
        v_num = collatz_eval(201, 210)
        self.assertEqual(v_num, 89)

    def test_eval_4(self):
        """
        Test 4
        """
        v_num = collatz_eval(900, 1000)
        self.assertEqual(v_num, 174)

    def test_eval_5(self):
        """
        Test 5
        """
        v_num = collatz_eval(948124, 40410)
        self.assertEqual(v_num, 525)

    def test_eval_6(self):
        """
        Test 6
        """
        v_num = collatz_eval(1, 999999)
        self.assertEqual(v_num, 525)

    def test_eval_7(self):
        """
        Test 7
        """
        v_num = collatz_eval(400, 1600)
        self.assertEqual(v_num, 182)

    def test_eval_8(self):
        """
        Test 8
        """
        v_num = collatz_eval(97842, 3)
        self.assertEqual(v_num, 351)

    def test_eval_9(self):
        """
        Test 9
        """
        v_num = collatz_eval(329475, 523300)
        self.assertEqual(v_num, 470)

    def test_eval_10(self):
        """
        Test 10
        """
        v_num = collatz_eval(5340, 134600)
        self.assertEqual(v_num, 354)

    def test_eval_11(self):
        """
        Test 11
        """
        v_num = collatz_eval(346330, 108900)
        self.assertEqual(v_num, 443)

    def test_eval_12(self):
        """
        Test 12
        """
        v_num = collatz_eval(15235, 100550)
        self.assertEqual(v_num, 351)

    def test_eval_13(self):
        """
        Test 13
        """
        v_num = collatz_eval(1, 2)
        self.assertEqual(v_num, 2)

    def test_eval_14(self):
        """
        Test 14
        """
        v_num = collatz_eval(245, 276)
        self.assertEqual(v_num, 123)

    def test_eval_15(self):
        """
        Test 15
        """
        v_num = collatz_eval(60, 838)
        self.assertEqual(v_num, 171)

    def test_eval_16(self):
        """
        Test 16
        """
        v_num = collatz_eval(5, 562)
        self.assertEqual(v_num, 144)

    def test_eval_17(self):
        """
        Test 17
        """
        v_num = collatz_eval(1, 999)
        self.assertEqual(v_num, 179)

    # -----
    # print
    # -----

    def test_print(self):
        """
        Test for print
        """
        w_num = StringIO()
        collatz_print(w_num, 1, 10, 20)
        self.assertEqual(w_num.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        """
        Test for solve
        """
        r_num = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w_num = StringIO()
        collatz_solve(r_num, w_num)
        self.assertEqual(
            w_num.getvalue(),
            "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

# ----
# main
# ----


if __name__ == "__main__":  # pragma: no cover
    main()
